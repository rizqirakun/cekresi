'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
 
gulp.task('sass-compile', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('browserSync', function() {
   browserSync.init({
        server: "./"
    });

})
 
gulp.task('watch', ['browserSync', 'sass-compile'], function (){
  gulp.watch('**/sass/**/*.+(scss|sass)', ['sass-compile']); 
  gulp.watch("*.html").on('change', browserSync.reload);
  gulp.watch('**/js/**/*.js', browserSync.reload); 

});